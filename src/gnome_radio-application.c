/* gnome_radio-application.c
 *
 * Copyright 2022 Ole Aamot
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <gst/player/player.h>
#include "gnome_radio-application.h"
#include "gnome_radio-window.h"

struct _GnomeRadioApplication
{
  GtkApplication parent_instance;
};

G_DEFINE_TYPE (GnomeRadioApplication, gnome_radio_application, ADW_TYPE_APPLICATION)

GnomeRadioApplication *
gnome_radio_application_new (gchar *application_id,
                             GApplicationFlags  flags)
{
  return g_object_new (GNOME_RADIO_TYPE_APPLICATION,
                       "application-id", application_id,
                       "flags", flags,
                       NULL);
}

static void
gnome_radio_application_finalize (GObject *object)
{
  GnomeRadioApplication *self = (GnomeRadioApplication *)object;

  G_OBJECT_CLASS (gnome_radio_application_parent_class)->finalize (object);
}

static void
gnome_radio_application_activate (GApplication *app)
{
  GtkWindow *window;
  GstPlayer *player;

  /* It's good practice to check your parameters at the beginning of the
   * function. It helps catch errors early and in development instead of
   * by your users.
   */
  g_assert (GTK_IS_APPLICATION (app));

  /* Get the current window or create one if necessary. */
  window = gtk_application_get_active_window (GTK_APPLICATION (app));
  if (window == NULL)
    window = g_object_new (GNOME_RADIO_TYPE_WINDOW,
                           "application", app,
                           NULL);
  gtk_window_set_application (GTK_WINDOW(window), GTK_APPLICATION(app));
  gtk_window_set_title(GTK_WINDOW(window), "Radio");
  gtk_window_set_default_size(GTK_WINDOW(window), 800, 600);
  /* Ask the window manager/compositor to present the window. */
  gtk_window_present (window);

  player = gst_player_new (NULL, gst_player_g_main_context_signal_dispatcher_new(NULL));
  gst_player_set_uri (GST_PLAYER (player), "http://stream.live.vc.bbcmedia.co.uk/bbc_world_service");
  gst_player_play (GST_PLAYER (player));
}


static void
gnome_radio_application_class_init (GnomeRadioApplicationClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GApplicationClass *app_class = G_APPLICATION_CLASS (klass);

  object_class->finalize = gnome_radio_application_finalize;

  /*
   * We connect to the activate callback to create a window when the application
   * has been launched. Additionally, this callback notifies us when the user
   * tries to launch a "second instance" of the application. When they try
   * to do that, we'll just present any existing window.
   */
  app_class->activate = gnome_radio_application_activate;
}

static void
gnome_radio_application_show_about (GSimpleAction *action,
                                    GVariant      *parameter,
                                    gpointer       user_data)
{
  GnomeRadioApplication *self = GNOME_RADIO_APPLICATION (user_data);
  GtkWindow *window = NULL;
  const gchar *authors[] = {"Ole Aamot", NULL};

  g_return_if_fail (GNOME_RADIO_IS_APPLICATION (self));

  window = gtk_application_get_active_window (GTK_APPLICATION (self));

  gtk_show_about_dialog (window,
                         "program-name", "Radio",
                         "authors", authors,
                         "version", "0.1.0",
                         NULL);
}


static void
gnome_radio_application_init (GnomeRadioApplication *self)
{
  g_autoptr (GSimpleAction) quit_action = g_simple_action_new ("quit", NULL);
  g_signal_connect_swapped (quit_action, "activate", G_CALLBACK (g_application_quit), self);
  g_action_map_add_action (G_ACTION_MAP (self), G_ACTION (quit_action));

  g_autoptr (GSimpleAction) about_action = g_simple_action_new ("about", NULL);
  g_signal_connect (about_action, "activate", G_CALLBACK (gnome_radio_application_show_about), self);
  g_action_map_add_action (G_ACTION_MAP (self), G_ACTION (about_action));

  gtk_application_set_accels_for_action (GTK_APPLICATION (self),
                                         "app.quit",
                                         (const char *[]) {
                                           "<primary>q",
                                           NULL,
                                         });
  gtk_application_set_accels_for_action (GTK_APPLICATION (self),
                                         "app.about",
                                         (const char *[]) {
                                           "<primary>a",
                                           NULL,
                                         });
}
